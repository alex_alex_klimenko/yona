<?php
/**
 * @author Oleksandr Torosh <webtorua@gmail.com>
 */

namespace Yona\Cache;

class Keys
{
    const ASSETS_HASH = 'Assets hash';
    const PAGE = 'PAGE';
    const PUBLICATION = 'PUBLICATION';
}