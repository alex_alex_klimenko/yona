# Assets

Assets sources placed on app level to prevent application scripts sources downloading by third persons.
All assets compiles by webpack and places to `public/dist` directory as uglified minimized files.